# Inertial Wrench Compensation - README

## Overview
This software provides a node to dynamically compensate the inertial wrench of a load mounted to a force-torque (FT) sensor, as well as the sensor's bias for force and torque.
The compensator relies solely on measurements of force, torque, linear acceleration and angular velocity, allowing it to perform inertia estimation on any robotic platform that incorporates a Bota Systems EtherCAT sensor or similar force–torque/IMU sensor.
The output of this node is the wrench coming only from contacts or (yet) unidentified payloads.
The node provides an interface for loading the inertial properties from a file OR to estimate it while moving the sensor with its payload along a trajectory without contacts.

The inertial_wrench_compensation package has been tested under ROS Noetic and Ubuntu 20.04. This is research code, expect that it often changes and any fitness for a particular purpose is disclaimed.

**Author(s):** André Kissling, Martin Wermelinger

## Description

The aim of this software is to provide a simple–to–use procedure to identify the mass, center of gravity, and moment of inertia of the load mounted onto a force–torque sensor, as well as the force and torque bias of the sensor.
It has two main components:
- **Estimation** of inertial properties and sensor bias.
- Dynamic **Compensation** of inertial forces and sensor bias.

Elimination of inertial forces allows observing interaction forces without any obstruction by the end–effector’s inertial properties.
Such a feature provides more precise feedback for control processes and allows fast detection of unintended contact, a critical feature in collaborative robotics.
The inertial properties and sensor bias might frequently differ due to changing payload, change of environmental conditions (e.g. temperature), pretensioning of the sensor while mounting it.
This package provides an easy routine to update changing inertial properties and compensate for them.

## Installation

### Cloning the package

Clone the package with:

```bash
git clone https://gitlab.com/botasys/inertial_wrench_compensation_demo.git
```

### Building from Source

In order to use the `inertial_wrench_estimation_demo` package, you need to install the core dependencies from a debian package located inside `inertial_wrench_estimation_demo/debian`:
```
sudo dpkg -i ros-noetic-inertial-wrench-compensation_1.0.0-0focal_amd64.deb
```
**Note:** Choose the correct for your distribution. Currently, only Ubuntu 20.04 and ROS noetic is supported.  

#### Dependencies

After cloning the package and before building, you need also to make sure that all the binary dependencies are installed. To do so, run in a terminal:

```bash
cd catkin_workspace/ && rosdep update && rosdep install --from-path src --ignore-src -y
```

#### Building

Compile the package using the `catkin_tools` package:

```bash
cd catkin_workspace
catkin build inertial_wrench_compensation_demo
```

## Usage

### Launching

Run the node with:

```bash
    roslaunch inertial_wrench_compensation_demo inertial_wrench_compensation.launch
```

### Configuration
The parameters are configured to be used with a Bota Systems Rokubi EtherCAT sensor.

The following parameter files are loaded with the launch file:

[`inertial_properties.yaml`](https://gitlab.com/botasys/inertial_wrench_compensation_demo/-/blob/main/config/compensation/inertial_properties.yaml) **Note:** the inertial properties are overwritten when the estimator is run successfully.
- inertial_parameters/mass: payload's mass
- inertial_parameters/com: payload's center of mass in the form [cx, cy, cz]
- inertial_parameters/inertia: payload's inertia in the form [Ixx, Ixy, Ixz, Iyy, Iyz, Izz]

[`calibration_bias.yaml`](https://gitlab.com/botasys/inertial_wrench_compensation_demo/-/blob/main/config/compensation/calibration_bias.yaml) **Note:** the calibration bias are overwritten when the estimator is run successfully.
- calibration/bias/force: force bias in the form [fx, fy, fz]
- calibration/bias/torque: torque bias in the form [tx, ty, tz]
- calibration/bias/linAcc: linear acceleration bias in the form [ax, ay, az]
- calibration/bias/angVel: angular rate bias in the form [wx, wy, wz]

[`inertia_compensated_wrench.yaml`](https://gitlab.com/botasys/inertial_wrench_compensation_demo/-/blob/main/config/compensation/inertia_compensated_wrench.yaml)
- sensor_name: sensor name (to lookup tfs)
- compensator/algorithm: compensation algorithm (option: `inertiaBias`, `gravityBias`, `inertiaBiasWoDelay`)
- subscribed/reading: sensor reading topic
- published/wrench/calibrated: calibrated wrench topic
- published/wrench/compensated: compensated wrench topic
- calibration/delay/linAcc: delay of linear acceleration filter [s]
- calibration/delay/angVel: delay of angular rate filter [s]
- calibration/delay/wrench: delay wrench measurements [s]
- min_runtime: minimum estimator runtime before convergence
  max_runtime: maximum estimator runtime before abortion
- filter/coeffs/smoothing/imu: imu smoothing filter coefficients
- filter/coeffs/smoothing/ft: wrench smoothing filter coefficients
- filter/coeffs/differentiation/ang_vel: angular rate differentiation filter coefficients

[`estimation.yaml`](https://gitlab.com/botasys/inertial_wrench_compensation_demo/-/blob/main/config/estimation/estimation.yaml)
- algorithm: estimation algorithm (option: `recursiveTLS`, `recursiveTLSWrenchBias`, `recursiveTLSWrenchImuBias`)
- lambda: exponential forgetting factor
- prior/mass: estimator prior for payload's mass
- prior/com: estimator prior for payload's center of mass in the form [cx, cy, cz]
- prior/vechI: estimator prior for payload's inertia in the form [Ixx, Ixy, Ixz, Iyy, Iyz, Izz]
- variance/imu/linAcc: sensor variance linear acceleration
- variance/imu/angVel: sensor variance angular rate
- variance/imu/angAcc: sensor variance rotational acceleration
- variance/wrench/force: sensor variance force readings
- variance/wrench/torque: sensor variance torque readings
- convergence/threshold/mass: mass covariance threshold for estimator convergence
- convergence/threshold/com: center of mass covariance threshold for estimator convergence
- convergence/threshold/inertia: inertial parameters covariance threshold for estimator convergence
- convergence/threshold/wrench_bias: wrench bias covariance threshold for estimator convergence

### Subscribed Topics

* Sensor IMU readings ([`sensor_msgs/Imu`](http://docs.ros.org/noetic/api/sensor_msgs/html/msg/Imu.html))
* Sensor wrench measurements ([`geometry_msgs/WrenchStamped`](http://docs.ros.org/noetic/api/geometry_msgs/html/msg/WrenchStamped.html))

### Published Topics

* **`/inertial_wrench_compensation/wrench_calibrated`** ([`geometry_msgs/WrenchStamped`](http://docs.ros.org/noetic/api/geometry_msgs/html/msg/WrenchStamped.html))
* **`/inertial_wrench_compensation/wrench_compensated`** ([`geometry_msgs/WrenchStamped`](http://docs.ros.org/noetic/api/geometry_msgs/html/msg/WrenchStamped.html))

### Services

Start / Stop the estimator:
* **`/inertial_wrench_compensation/start_estimator`** ([`std_srvs/Trigger`](http://docs.ros.org/en/noetic/api/std_srvs/html/srv/Trigger.html))

**Note:** Make sure that the payload is not in contact with anything except the sensor while the estimator is running. The estimator stops by itself either after convergence or after a time-out

## Topic Averaging

To obtain the wrench & IMU bias of the sensor put it on a horizontal surface without mounting any payload.

### Launching

Run the node with:

```bash
    roslaunch inertial_wrench_compensation_demo topic_average.launch
```